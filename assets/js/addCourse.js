const token = localStorage.getItem("token");

// let courseForm = document.getElementById("courseForm");

// courseForm.addEventListener("submit", (form) => {
// 	form.preventDefault();

// 	let courseName = document.getElementById("courseName").value;
// 	let courseDesc = document.getElementById("courseDescription").value;
// 	let coursePrice = document.getElementById("coursePrice").value;

// 	fetch('http://localhost:5000/api/courses', {
// 	// fetch('http://radiant-badlands-29813.herokuapp.com/api/courses', {
// 		method: 'POST',
// 		headers: {
// 			'Content-Type': 'application/json',
// 			'Authorization': `Bearer ${token}`
// 		},
// 		body: JSON.stringify({
// 	   	   name: courseName,
// 	   	   description: courseDesc,
// 	   	   price: coursePrice,
// 		})
// 	})
// 	.then(res => {
// 		return res.json()
// 	})
// 	.then(data => {
// 		console.log(data)
// 		if(data === true){
// 			alert("New course has been added successfully.")
// 			// window.location.replace('./profile.html')
// 		}else{
// 			alert("Something went wrong. Course wasn't added to the database.")
// 		}
// 	})
// })

 function readURL(input) {
        if (input.files && input.files[0]) {
        	let courseImage = document.getElementById("courseImage").files[0];

        	console.log(courseImage)

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadImage').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }