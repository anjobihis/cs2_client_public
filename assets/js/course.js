let _courseId = sessionStorage.getItem("courseId")
let _isAdmin = localStorage.getItem("isAdmin")
let _userId = localStorage.getItem("id")
const token = localStorage.getItem("token")
let appendable = document.getElementById("appendable");

let profileTab = document.querySelector('.profile');

if(_isAdmin === 'true'){
	profileTab.innerHTML = "Add Course"
}

function convertDate(enrolledOn) {
	const date = ("0"+(enrolledOn.getMonth()+1)).slice(-2) + '-' + ("0"+(enrolledOn.getDate())).slice(-2) + '-' + enrolledOn.getFullYear();
	// const time = (enrolledOn.getHours() + ':' + (enrolledOn.getMinutes()+"0").slice(-2))
  	return  {date}
}

if(_courseId !== null){
	if(_isAdmin === 'true'){
		// fetch(`http://localhost:5000/api/courses/${_courseId}`, {
		fetch(`http://radiant-badlands-29813.herokuapp.com/api/courses/${_courseId}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => {
			return res.json()
		})
		.then(course => {
			let courseName = document.getElementById("courseName")
			let courseDescription = document.getElementById("courseDescription")
			let coursePrice = document.getElementById("coursePrice")

			courseName.innerHTML = course.name
			courseDescription.innerHTML = course.description
			coursePrice.innerHTML = course.price

			let isChecked = "";

			if(course.isActive) isChecked = "checked";

			let adminAccess = 
			`
			<section class="text-center">
				<h2 class="text-dark" id="courseIsActive">Course Status: ${isActive(course.isActive)}</h2>
				<label class="switch">
				  <input type="checkbox" id="archiveBtn" ${isChecked}>
				  <span class="slider round"></span>
				</label>
			</section>
			`;

			let enrolleeList = course.enrollees.map(x => {
				let getDate = new Date(x.enrolledOn)
				let enrolledOn = convertDate(getDate)
					return `
					    </tr>
							<td class="text-center">${x.firstName}</td>
							<td class="text-center">${x.lastName}</td>
							<td class="text-center">${x.email}</td>
							<td class="text-center">${enrolledOn.date}</td>
						</tr>
				`}).join('')

			let enrolledStudents = 			
			`<div class="col-md-8 pb-5">
				<section class="my-5">
				<h2 class="text-dark">ENROLLED STUDENTS</h2>
					<table class="table table-dark">
						<thead>
							<tr>
								<th class="text-center">First Name</th>
								<th class="text-center">Last Name</th>
								<th class="text-center">E-mail</th>
								<th class="text-center">Enrolled On</th>
							</tr>
							<tbody>
								${enrolleeList}
							</tbody>
						</thead>
					</table>
				</section>
			</div>`

			appendable.innerHTML = adminAccess;
			appendable.innerHTML += enrolledStudents;

			let archiveBtn = document.querySelector("#archiveBtn");
			let courseIsActive = document.getElementById("courseIsActive");

			archiveBtn.addEventListener("click", () => {
				if(archiveBtn.checked === true){
					// fetch(`http://localhost:5000/api/courses/${_courseId}`, {
					fetch(`http://radiant-badlands-29813.herokuapp.com/api/courses/${_courseId}`, {
							method: "PUT",
							headers: {
								"Content-Type": "application/json",
								"Authorization": `Bearer ${token}`
							},
							body: JSON.stringify({
								isActive: true
							})
						})
						.then(res => {
							return res.json()
						})
					courseIsActive.innerHTML = "Course Status: ACTIVE"	
				}
				else{
					// fetch(`http://localhost:5000/api/courses/${_courseId}`, {
					fetch(`http://radiant-badlands-29813.herokuapp.com/api/courses/${_courseId}`, {
							method: "DELETE",
							headers: {
								"Content-Type": "application/json",
								"Authorization": `Bearer ${token}`
							}
						})
						.then(res => {
							return res.json()
						})
					courseIsActive.innerHTML = "Course Status: INACTIVE"	
				}
			})
		})
	//IF NOT ADMIN
	}else{
		// fetch(`http://localhost:5000/api/courses/${_courseId}`, {
		fetch(`http://radiant-badlands-29813.herokuapp.com/api/courses/${_courseId}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => {
			return res.json()
		})
		.then(course => {
			let courseName = document.getElementById("courseName")
			let courseDescription = document.getElementById("courseDescription")
			let coursePrice = document.getElementById("coursePrice")

			courseName.innerHTML = course.name
			courseDescription.innerHTML = course.description
			coursePrice.innerHTML = course.price

			let studentAccess = 
								`
								<section class="text-center">
									<button class="btn btn-primary btn-lg" id="enrollBtn">Enroll Course</button>
								</section>
								`;
			appendable.innerHTML = studentAccess;
			enrollBtn.addEventListener("click", () => {
			// fetch('http://localhost:5000/api/users/enroll', {
			fetch(`http://radiant-badlands-29813.herokuapp.com/api/users/enroll`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: course._id,
					courseName: course.name
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					if(data === true){
						alert("Course is now enrolled.")
					}else{
						alert("Something went wrong. Course enrollment failed.")
					}
				})
			})
		})

	}

}else{
	window.location.replace('./courses.html')
}


function isActive(bool){
	if(bool){
		return "ACTIVE"
	}else{
		return "INACTIVE"
	}
}