//-------------------------------------------GLOBAL VARIABLES-------------------------------------------------------------
const token = localStorage.getItem('token')

let main = document.getElementById('main')

let viewType = document.getElementById('adminAccess')

let _isAdmin = localStorage.getItem('isAdmin')

let _activeCards = []

let isChecked = ''

let statusId = 0

//---------------------------------------------FUNCTIONS------------------------------------------------------------------
function pushCards(courses) {
  for (let i = 0; i < courses.length; i++) {
    const courseFiles = courses.map((course) => course.name.toLowerCase())
    console.log(courseFiles[i])

    let card = `<div class="card text-center shadow" id="courseContainer">
			<a href="./course.html">
				<img class="card-img-top visual" src="../assets/images/${courseFiles[i]}.jpg" id="${courses[i].name}">
			</a>
			<h4>${courses[i].name}</h4>
			<p class="card-text">${courses[i].description}</p>`

    _activeCards.push(card)
  }
}

function populateCards(courses) {
  let course = document.querySelector('.card-deck')
  if (_isAdmin === 'true') {
    let textActive = ''
    for (let i = 0; i < _activeCards.length; i++) {
      if (courses[i].isActive) {
        textActive = 'ACTIVE'
      } else {
        textActive = 'INACTIVE'
      }

      if (courses[i].isActive) {
        isChecked = 'checked'
      } else {
        isChecked = ''
      }

      _activeCards[i] += `
								<h5 id="${statusId}">${textActive}</h5>
								<div class="text-center">
								<label class="switch">
								  <input type="checkbox" id="archiveBtn" ${isChecked}>
								  <span class="slider round"></span>
								</label>
								</div>
								</div>
								`

      course.innerHTML += _activeCards[i]
    }

    viewType.innerHTML = 'Administrator'
  } else {
    for (let i = 0; i < _activeCards.length; i++) {
      _activeCards[i] += `<h5 id="courseStatus">$ ${courses[i].price}</h5>
								</div>`
      course.innerHTML += _activeCards[i]
    }
    viewType.innerHTML = 'Choose a course:'
  }

  for (let i = 0; i < _activeCards.length; i++) {
    let courseLink = document.getElementById(`${courses[i].name}`)
    courseLink.addEventListener('click', () => {
      sessionStorage.setItem('courseId', courses[i]._id)
    })
  }
}

function isActive(bool) {
  if (bool) {
    return 'ACTIVE'
  } else {
    return 'INACTIVE'
  }
}

//---------------------------------------------PAGE LOGIC------------------------------------------------------------------

let profileTab = document.querySelector('.profile')

if (_isAdmin === 'true') {
  profileTab.innerHTML = 'Add Course'
}

main.innerHTML = `
					<section class="container-fluid padding cards-container my-5 pb-5">
						<div class="row padding text-dark">

							<div class="card-deck parent mb-5">
								
							</div>
						</div>
					</section>
				`

if (!token || token === null) {
  alert('You must be logged in to do that.')
  window.location.replace('./login.html')
} else {
  if (_isAdmin === 'true') {
    // fetch('http://localhost:5000/api/courses/admin', {
    fetch('http://radiant-badlands-29813.herokuapp.com/api/courses/admin', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((res) => {
        return res.json()
      })
      .then((courses) => {
        pushCards(courses)
        populateCards(courses)
      })
  } else {
    // fetch('http://localhost:5000/api/courses/user', {
    fetch('http://radiant-badlands-29813.herokuapp.com/api/courses/user', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((res) => {
        return res.json()
      })
      .then((courses) => {
        pushCards(courses)
        populateCards(courses)
      })
  }
}
