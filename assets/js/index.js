const token = localStorage.getItem("token")
const isAdmin = localStorage.getItem("isAdmin")

let showProfile = document.getElementById("showProfile")
let signIn = document.getElementById("signIn")

let profileNav
let signInNav

if(isAdmin === 'true'){
	profileNav =	`<li class="nav-item">
						<a href="./pages/profile.html" class="nav-link"> Add Course </a>
					</li>`

	signInNav =		`<li class="nav-item">
						<a href="./pages/logout.html" class="nav-link"> Log Out </a>
					</li>`
}else{
	profileNav =	`<li class="nav-item">
						<a href="./pages/profile.html" class="nav-link"> Profile </a>
					</li>`

	signInNav =		`<li class="nav-item">
						<a href="./pages/logout.html" class="nav-link"> Log Out </a>
					</li>`
}


if(!token || token === null){

	profileNav =	`<li class="nav-item">
						<a href="./pages/register.html" class="nav-link"> Register </a>
					</li>`

	signInNav =		`<li class="nav-item">
						<a href="./pages/login.html" class="nav-link"> Sign In </a>
					</li>`

}

showProfile.innerHTML = profileNav;
signIn.innerHTML = signInNav;
