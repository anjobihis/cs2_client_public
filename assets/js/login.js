// console.log('hello');

let loginForm = document.querySelector("#loginUser");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	//console.log(email);
	//console.log(password);
	//create a validation that will allow us to determine if
	//the input fields are not empty 
	if(email == '' || password == ''){
		alert("Please input email and/or password!");
	}else
	{
		// fetch('http://localhost:5000/api/users/login', {
		fetch('http://radiant-badlands-29813.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				//to check if the data has been caught, display it first in the console.
				//upon successful authentication it will return a JWT
				//create a control structure to determine if the JWT has been generated
				//together with the user credentials in an object format.
				if(data.loginType === "google"){
					return alert("This e-mail is used for google login.")
				}else if(data === false){
					return alert("Email address or password is incorrect.")
				}else{
					if(data.access){
						//we are going to store now the JWT inside our local storage.
						localStorage.setItem('token', data.access);
						//once that the id is authenticated successfully using the access token
						//then it should redirect the user to the user's profile page
		                // fetch(`http://localhost:5000/api/users/details`, {
		                fetch(`http://radiant-badlands-29813.herokuapp.com/api/users/details`, {
		                		method: "GET",
			                    headers: {
			                        Authorization: `Bearer ${data.access}`
			                    }
			                })
			                .then(res => {
			                    return res.json()
			                })
			                .then(data => {
			                    //set the global user state to have properties containing authenticated user ID and Role.
			                    localStorage.setItem("id", data._id)
			                    localStorage.setItem("isAdmin", data.isAdmin)
			                    //once that the id and is admin property of the user is succefully saved in the local storage, then the next task is to redirect the user to the profile page. 
			                    window.location.replace("./profile.html")
			                })	
					}else{
						alert("Something went wrong, check your credentials!");
					}
				}

			})
	}
});