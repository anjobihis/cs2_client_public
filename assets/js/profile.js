const token = localStorage.getItem("token");

const _isAdmin = localStorage.getItem('isAdmin');

let profile = document.getElementById("profileContainer");

let courseNames = [];

let profileTab = document.querySelector('.profile');

if(_isAdmin === 'true'){
	profileTab.innerHTML = "Add Course"
}

function addEnrolledCourses(courses){
	console.log("running addEnrolledCourses")
	console.log(courses.name)
	console.log(`courses.length = ${courses.length}`)
}

function convertDate(enrolledOn) {
  return ("0"+(enrolledOn.getMonth()+1)).slice(-2) + '-' + ("0"+(enrolledOn.getDate())).slice(-2) + '-' + enrolledOn.getFullYear();
}



if(!token || token === null){
	alert("You must be logged in to do that.")
	window.location.replace("./login.html")
}else{
	// fetch("http://localhost:5000/api/users/details", {
	fetch("http://radiant-badlands-29813.herokuapp.com/api/users/details", {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		}
	})
	.then(res => {
		return res.json();
	})
	.then(data => {
		if(data.isAdmin){
			window.location.replace('./add-course.html');
		}else{
			let enrollmentData = data.enrollments.map(x => {
				let enrolledOn = new Date(x.enrolledOn)
				let date = convertDate(enrolledOn)
					return `
					    </tr>
							<td>${x.courseName}</td>
							<td>${date}</td>
							<td>${x.status}</td>
						</tr>
				`}).join('')

			profile.innerHTML = `
								<div class="col-md-12">
									<section class="jumbotron my-5">
										<h3 class="text-left text-dark">First Name: ${data.firstName}</h3>
										<h3 class="text-left text-dark">Last Name: ${data.lastName}</h3>
										<h3 class="text-left text-dark">Email: ${data.email}</h3>
										<h3 class="text-center text-dark">Courses Enrolled</h3>
										<table class="table table-dark">
											<thead>
												<tr>
													<th> Course Name </th>
													<th> Enrolled On </th>
													<th> Status </th>
												</tr>
												<tbody>
													${enrollmentData}
												</tbody>
											</thead>
										</table>
									</section>
								</div>

								`
		}
	})
}
