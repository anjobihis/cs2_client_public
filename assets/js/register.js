// console.log("hello from register");

let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#userEmail").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	//create a validation to enable the submit button when all fields
	//are registered and if both passwords match
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11))
	{
		//if all requirements are met, then now lets check for duplicate emails
		// fetch('http://localhost:5000/api/users/email-exists', {
		fetch(`http://radiant-badlands-29813.herokuapp.com/api/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === false){
				// fetch('http://localhost:5000/api/users', {
				fetch(`http://radiant-badlands-29813.herokuapp.com/api/users`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json', 
					},
					body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					})
					.then(res => { 
						//NOTES:
						//res.send function sets the content type to text/html
						//which means that the client will now treat the response
						//as a text object. it then returns a response to the client
						return res.json()
					})
					.then(data => {
						console.log(data)
						//give proper response if the registration is successful
						if(data === true){
							alert("New account has been registered successfully.")
							//after confirmation of the alert window, lets redirect
							//the user to the login page.
							window.location.replace("./login.html")
						}else{
							alert("Something went wrong in the registration.")
						}
					})
			}else{
				alert("E-mail address already taken.")
			}
		})
	}else{
		alert("Something went wrong, check your credentials.")
	}
})